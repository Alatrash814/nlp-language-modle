import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.util.Pair;


public class HW2 {

    public static HashMap <String,Integer> statisitcs = new HashMap<>();
    public static Vector <String> allWord = new Vector<>();
    public static Pair <String,String> temp = new Pair <>("","");
    public static String firstKey = "";
    public static String secondKey = "";
    public static Integer count = 0;
    public static HashMap <Pair <String,String> , Integer> probabilities = new HashMap <>(); 
    public static FileInputStream content;
    public static FileOutputStream stemmedContent;
    public static FileOutputStream allFileStemmed;
    public static StringBuilder stringBuilder;
    public static Float value;
    public static Double prob;
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        System.out.println("If you want to train this program agine , please enter 1 ");
        System.out.println("If you Want to use this program to predict a sentence , please enter 2");
        System.out.print("Please enter your choice : ");
        
        Scanner scanner = new Scanner (System.in);
        int choice = scanner.nextInt();
        
        
        if (choice == 1){
            
            File directory = new File ("training");
            
            stemmedContent = new FileOutputStream("stemmed-content.txt");
            
            FileOutputStream wordContent = new FileOutputStream ("word-content.txt");
            FileOutputStream wordContent3 = new FileOutputStream ("word-content3.txt");
            
            System.out.println("Now read training data set");
            
            filesInDir(directory);
        
            Set <String> setString = statisitcs.keySet();
            
            Double sumOfProb = (double)statisitcs.size();
            
            System.out.println("Now write all words on a file with it's statistics");
            
            for (String currentWord : setString){
            
                wordContent3.write(currentWord.getBytes());
                wordContent3.write(" : ".getBytes());
                wordContent3.write(statisitcs.get(currentWord).toString().getBytes());
                wordContent3.write(System.lineSeparator().getBytes());
            
                                                }//for loop (to write all stemmed words on a flie called word-content3.txt)
        
            System.out.println("Now find all pairs statistics");
            
            for (int i = 1 ; i < allWord.size() ; i++){
            
                temp = new Pair <> (allWord.get(i-1),allWord.get(i));
            
                count = probabilities.get(temp);
            
                if (count == null)
                    probabilities.put(new Pair<> (allWord.get(i-1) , allWord.get(i)), 1);
            
                else
                    probabilities.put(new Pair<> (allWord.get(i-1) , allWord.get(i)), count + 1);
                
                                                    }//for loop (to put all words in a vector to use them to calculate probability) 
            
            System.out.println("Now write all pairs on a file with it's statistics");
            
            for (Pair <String,String> name: probabilities.keySet()){

                firstKey = name.getKey();
                secondKey = name.getValue();
                value = (float)probabilities.get(name);
                prob = ((value + 1) * (statisitcs.get(firstKey))) / (statisitcs.get(firstKey) + sumOfProb);
                wordContent.write(firstKey.getBytes()); 
                wordContent.write(" , ".getBytes());
                wordContent.write(secondKey.getBytes());
                wordContent.write(" : ".getBytes());
                wordContent.write(prob.toString().getBytes());
                wordContent.write(System.lineSeparator().getBytes());
            
                                                                    }//for loop (to write uni-gram with probability on file called word-content.txt)
        
            stemmedContent.flush();
            stemmedContent.close();
            wordContent.flush();
            wordContent.close();
            
                    }//if statement 
        
        
        else if (choice == 2){
        
            HashMap <Pair<String,String>,Double> probValue = new HashMap<>();
        
            File wordContent3 = new File ("word-content3.txt");
        
            File wordContent = new File ("word-content.txt");
        
            File content = new File ("content.txt");
            
            if (wordContent.exists() && wordContent.length() != 0 && wordContent3.exists() && wordContent3.length() != 0) {
        
                FileInputStream probValueText = new FileInputStream(wordContent);
        
                FileInputStream wordCounter = new FileInputStream ("word-content3.txt");
        
                BufferedReader wordCounterValueText = new BufferedReader(new InputStreamReader (wordCounter));
        
                BufferedReader probValueTextReader = new BufferedReader(new InputStreamReader(probValueText));
        
                String readLineByLine = wordCounterValueText.readLine();
        
                String checkRegex;
                Pattern patternRegex;
                Matcher matcherRegex;
        
                Integer counter;
        
                System.out.println("Now read probability for each word ");
                
                while (readLineByLine != null){
                    
                    checkRegex = "([a-z]+[\\s][:][\\s])";
                    patternRegex = Pattern.compile(checkRegex);
                    matcherRegex = patternRegex.matcher(readLineByLine);
                    
                    matcherRegex.find();
                    firstKey = matcherRegex.group();
                    firstKey = firstKey.substring(0,firstKey.length()-3);
                    
                    checkRegex = "(\\d+)";
                    patternRegex = Pattern.compile(checkRegex);
                    matcherRegex = patternRegex.matcher(readLineByLine);
                    
                    matcherRegex.find();
                    counter = Integer.parseInt(matcherRegex.group());
                    
                    statisitcs.put(firstKey, counter);
                    
                    readLineByLine = wordCounterValueText.readLine();
            
                                             }//while loop(to read stemmed word with it's counter)
            
                System.out.println("Now read probability for each pair ");
                                
                readLineByLine = probValueTextReader.readLine();
        
                while (readLineByLine != null){
            
                    checkRegex = "([a-z]+[\\s][,])";
                    patternRegex = Pattern.compile(checkRegex);
                    matcherRegex = patternRegex.matcher(readLineByLine);
            
                    matcherRegex.find();
                    firstKey = matcherRegex.group();
                    firstKey = firstKey.substring(0,firstKey.length()-2);
            
                    checkRegex = "([\\s][a-z]+[\\s][:][\\s])";
                    patternRegex = Pattern.compile(checkRegex);
                    matcherRegex = patternRegex.matcher(readLineByLine);
            
                    matcherRegex.find();
                    secondKey = matcherRegex.group();
                    secondKey = secondKey.substring(1, secondKey.length()-3);
            
                    checkRegex = "(\\d+\\.\\d+[E]?[-]?\\d?)";
                    patternRegex = Pattern.compile(checkRegex);
                    matcherRegex = patternRegex.matcher(readLineByLine);
            
                    matcherRegex.find();
                    prob = Double.parseDouble(matcherRegex.group());
            
                    probValue.put(new Pair <> (firstKey,secondKey), prob);
            
                    readLineByLine = probValueTextReader.readLine();
            
                                      }/*while loop (to read word-content.txt to a hash map with it's probability , to use them to calculate probability
                                        of next statement)*/
                
                FileInputStream contentFile = new FileInputStream(content);
                
                BufferedReader testDataReader = new BufferedReader(new InputStreamReader (contentFile));
                
                Vector <String> stemmedVector;
                
                String statementToPredect = testDataReader.readLine();
        
                Double sumOfProb = (double)statisitcs.size();
                
                Double numberOfWords = 0D;
                
                for (String Temp : statisitcs.keySet())
                    numberOfWords += statisitcs.get(Temp);
                
                while (statementToPredect != null){ 
                
                    statementToPredect += " ";
        
                    stemmedVector = stemToPredect(statementToPredect);
        
                    Double predValue = 0D;
        
                    Integer keyValue , valueOfKeyValue;
        
                    Double pairPred;
        
                    predValue += Math.log10(statisitcs.get(stemmedVector.get(0)) / numberOfWords);
                    
                    for (int i = 1 ; i < stemmedVector.size() ; i++){
            
                        firstKey = stemmedVector.get(i-1);
            
                        secondKey = stemmedVector.get(i);
            
                        keyValue = statisitcs.get(firstKey);
                    
                        valueOfKeyValue = statisitcs.get(secondKey);
            
                        if (keyValue == null){
                
                            keyValue = statisitcs.get("UNK");
                
                            if (keyValue == null)
                                statisitcs.put("UNK", 1);
                    
                            else
                                statisitcs.put("UNK", keyValue+1);
                
                            firstKey = "UNK";
                
                                            }//if statement (to handle unknow word problem)
            
                        if (valueOfKeyValue == null){
                
                            valueOfKeyValue = statisitcs.get ("UNK");
                
                            if (valueOfKeyValue == null)
                                statisitcs.put("UNK", 1);
                    
                            else    
                                statisitcs.put("UNK", keyValue + 1);
                
                            secondKey = "UNK";
                
                                                    }//if statement (to handle unknow word problem)
            
                    if (probValue.get(new Pair <> (firstKey,secondKey)) != null){
                        pairPred = (double)probValue.get(new Pair <> (firstKey,secondKey));
                                                                                }//if statement
            
                    else    
                        pairPred = (double) (1 * keyValue) / (keyValue + sumOfProb);
                
                        predValue += Math.log10(pairPred);
            
                                                                       }//for loop(to calculate predection)
        
                System.out.println("This statment : " + statementToPredect + " predection value is = " + predValue);
            
                statementToPredect = testDataReader.readLine();
            
                                                    }//while loop (to read input from file)
                                                                                                                            }//if statement
        
        else {
            
            System.out.println("The probability file does not exists , Please train this program to create this file !");
            System.exit(-1);//program will exit with "-1" to give the user notice that the program not run probably
            
             }//else statement
        
                               }//else if statement
        
                                                                                       }//main
    
    public static void filesInDir (File directory) throws IOException{
        File [] Temp = directory.listFiles();
        for (File Temp1 : Temp) 
            if (Temp1.isFile()) 
                stem(Temp1);
                                
             else 
                filesInDir(Temp1);
                                                                      }//filesInDir
    
    public static void stem (File file) throws IOException{
        
        Integer count = 0;
        
        int counter = 0;
        
        int sT;
        
        String temp = "";
        
        content = new FileInputStream(file);
        
        PorterStemmer stemmer = new PorterStemmer();
        
        while (true){
            
            sT = content.read();
            
            if (Character.isLetter((char)sT)){
                
                counter = 0;
                
                while (true){
                    
                    sT = Character.toLowerCase((char)sT);
                    temp += (char)sT;
                    if (counter < 500)
                        counter++;
                    sT = content.read();
                    
                    if (!Character.isLetter((char) sT)) {
                                                         
                        for (int c = 0; c < counter; c++)
                            stemmer.add(temp.charAt(c));
                        
                        stemmer.stem();
                        
                        if (!temp.equals("")){
                            
                            temp = stemmer.toString();
                            count = statisitcs.get(temp);
        
                            if (count == null){
                                
                                statisitcs.put(temp,1);
                                stemmedContent.write(temp.getBytes());
                                stemmedContent.write(System.lineSeparator().getBytes());
                                
                                              }//if statement
                            else
                                statisitcs.put(temp, count + 1);
                            
                            allWord.addElement(temp);
                            
                                             }//if statement
                        
                        temp = "";
                        break;
                                                        }//if statement
                    
                             }//while loop
                
                                               }//if statement
                    if (sT < 0) break;
                    
                       }//while loop
     
                                                            }//stem 
    
    public static Vector stemToPredect (String statemet){
        
        Vector <String> stemmedVector = new Vector<>();
        
        Integer count;
        
        int counter , sT;
        
        String temp = "";
        
        PorterStemmer stemmer = new PorterStemmer();
        
        for (int i = 0 ; i < statemet.length() ;){
            
            sT = statemet.charAt(i);
            
            if (!Character.isLetter(sT)){
                i++;
                continue;
                                        }//if statement
            
            if (Character.isLetter((char)sT)){
                
                counter = 0;
                
                while (i < statemet.length()){
                    
                    sT = Character.toLowerCase((char)sT);
                    temp += (char)sT;
                    if (counter < 500)
                        counter++;
                    i++;
                    sT = statemet.charAt(i);

                    if (!Character.isLetter((char) sT)) {
                                                         
                        for (int c = 0; c < counter; c++)
                            stemmer.add(temp.charAt(c));
                        
                        stemmer.stem();
                        
                        if (!temp.equals("")){
                            
                            temp = stemmer.toString();
                            
                            stemmedVector.addElement(temp);
                            
                                             }//if statement
                        
                        temp = "";
                        i++;
                        break;
                                                        }//if statement
                    
                             }//while loop
                
                                               }//if statement
            
                    if (sT < 0)
                        break;
                 
                              
                    
                       }//while loop
     
        
        
        return stemmedVector;
        
                                                        }//stemToPredect
    
                 }//HW2 class
